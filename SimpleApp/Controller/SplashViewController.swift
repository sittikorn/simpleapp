//
//  SplashViewController.swift
//  Nuclear
//
//  Created by Sittikorn on 12/13/2558 BE.
//  Copyright © 2558 Sittikorn Asavathavornvanich. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    @IBOutlet weak var progressView: UIProgressView!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // get all survey
        Survey.sharedInstance.getAllSurvay(self)
    }
    
    override func viewDidLayoutSubviews() {
        view.frame = UIScreen.mainScreen().bounds
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector:Selector("setProgress"), userInfo: nil, repeats: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


// MARK: - Survey Delegate
// ********************************** SURVEY DELEGATE **************************************** //
extension SplashViewController: SurveyDelegate {
    
    func SurveyGetAllData(data: AnyObject?) {
        
        if data != nil {

            progressView.progress = 1
            view.removeFromSuperview()
            NSNotificationCenter.defaultCenter().postNotificationName("reloadData", object: nil)
        }
    }
}
