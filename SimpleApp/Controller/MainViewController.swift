//
//  MainViewController.swift
//  SimpleApp
//
//  Created by Sittikorn on 1/4/2559 BE.
//  Copyright © 2559 Sittikorn. All rights reserved.
//

import UIKit
import iCarousel
import Kingfisher

class MainViewController: UIViewController {
    
    @IBOutlet weak var icarousel: iCarousel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var survayButton: UIButton!
    
    let imageArray = ["bg1","bg2","bg3"]
    let strings = Strings()
    
    private var surveyData:AnyObject?
    private var isLoadView = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createTitleView()
        createRightBar()
        createLeftBar()
        
        
        // set radius button
        survayButton.layer.cornerRadius = 20
        
        // set paging to vertical
        pageControl?.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_2));
        
        // setup icarousel
        icarousel?.type = iCarouselType.Linear
        icarousel?.pagingEnabled = true
        icarousel?.clipsToBounds = true
        icarousel.vertical = true
        
        // register nsnotification name : 'reloadData'
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "reloadData:", name:"reloadData", object: nil)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        pageControl.frame = CGRectMake(view.frame.size.width * 90 / 100, pageControl.frame.origin.y, pageControl.frame.size.width, pageControl.frame.size.height)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // ************************* SURVAY ACTION *********************Y******** //
    
    @IBAction func survayAction(sender: AnyObject) {
        
        let survey = UINavigationController(rootViewController: SurveyViewController(nibName: "SurveyViewController", bundle: nil))
        
        presentViewController(survey, animated: true, completion: nil)
    }
    
    // ************************* PAGE ACTION ***************************** //
    
    @IBAction func pageAction(sender: AnyObject) {
        
        icarousel.scrollToItemAtIndex(pageControl.currentPage, animated: true)
    }
    
    
    // ************************* CREATE TITLE VIEW ***************************** //
    
    func createTitleView() {
        
        let titleLabel = UILabel(frame: CGRectZero)
        titleLabel.text = "SURVEYS"
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.font = UIFont.boldSystemFontOfSize(22);
        titleLabel.sizeToFit()
        
        navigationItem.titleView = titleLabel
        
    }
    
    // ********************************** CRAETE LEFT BARBUTTON **************************************** //
    func createLeftBar() {
        
        let reloadButton = UIButton(frame: CGRectMake(0,0,20,20))
        reloadButton.setImage(UIImage(named: "refresh"), forState: UIControlState.Normal)
        reloadButton.tintColor = UIColor.whiteColor()
        reloadButton.addTarget(self, action: Selector("reloadAction:"), forControlEvents: UIControlEvents.TouchUpInside)
        
        let reloadBarButton = UIBarButtonItem(customView: reloadButton)
        
        navigationItem.leftBarButtonItem = reloadBarButton
        
    }
    
    func reloadAction(sender:UIButton) {
        
        // get all survey
        Survey.sharedInstance.getAllSurvay(self)
        
    }
    
    // ********************************** LOAD DATA **************************************** //
    func reloadData(notification:NSNotification){
        
        surveyData = Survey.sharedInstance.survayData
        
        pageControl.numberOfPages = surveyData!.count
        
        icarousel.reloadData()
        
        if surveyData != nil {
            
            // set title
            titleLabel.text = surveyData![0]["title"] as? String
            
            // set description
            descLabel.text = surveyData![0]["description"] as? String
            
        }
        
    }
    
    
    // ********************************** CRAETE RIGHT BARBUTTON **************************************** //
    func createRightBar() {
        
        let menuBar = UIButton(frame: CGRectMake(0,0,25,25))
        menuBar.setImage(UIImage(named: "categoryBtn"), forState: UIControlState.Normal)
        menuBar.tintColor = UIColor.whiteColor()
        menuBar.addTarget(self, action: Selector("menuAction:"), forControlEvents: UIControlEvents.TouchUpInside)
        
        let menuNavBar = UIBarButtonItem(customView: menuBar)
        
        navigationItem.rightBarButtonItem = menuNavBar
        
    }
    
    func menuAction(sender:UIButton) {
        
    }
    
}


// MARK: - iCarousel Delegate
// ********************************** ICAROUSEL DELEGATE,DATASOUCE **************************************** //

extension MainViewController: iCarouselDataSource {
    
    
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int
    {
        if surveyData != nil {
            return surveyData!.count
        } else {
            return imageArray.count
        }
    }
    
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView
    {
        
        var itemView:UIImageView
        
        if (view == nil) {
            
            itemView = UIImageView(frame:CGRectMake(0, 0, carousel.frame.size.width, carousel.frame.size.height))
            itemView.contentMode = UIViewContentMode.ScaleAspectFit

        } else {
            
            itemView = view as! UIImageView
//            itemView.frame = CGRectMake(0, 64, carousel.frame.size.width, carousel.frame.size.height)
        }
        
        
        if surveyData != nil {
            
            if surveyData?[index]["cover_background_color"] as? String != nil {
                
                if index == 4 {
                    carousel.backgroundColor = UIColor_HexString.colorFromHexString(strings.headerColor)
                } else {
                    carousel.backgroundColor = UIColor_HexString.colorFromHexString(surveyData![index]["cover_background_color"] as! String)
                }
            } else {
                carousel.backgroundColor = UIColor_HexString.colorFromHexString(strings.headerColor)
            }
            
            itemView.kf_setImageWithURL(NSURL(string: surveyData![index]["cover_image_url"] as! String)!,
                placeholderImage: UIImage(named: "Placeholder"),
                optionsInfo: [.Transition(ImageTransition.Fade(1))],
                progressBlock: { (receivedSize, totalSize) -> () in
                    //print("Download Progress: \(receivedSize)/\(totalSize)")
                }, completionHandler: { (image, error, cacheType, imageURL) -> () in
                    //print(image?.size.width)
                    //print(imageURL)
                    //print(index)
                    if image == nil {
                        itemView.image = UIImage(named: self.imageArray[0])
                    }
            })
        }
        
        //        itemView.image = UIImage(named: imageArray[index])
        
        return itemView
    }
    
    func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        switch option {
        case iCarouselOption.Wrap :
            return 1
        default: return value
        }
    }
    
    func carouselCurrentItemIndexDidChange(carousel: iCarousel) {
        
        pageControl.currentPage = carousel.currentItemIndex
        
        if surveyData != nil {
            
            // set title
            titleLabel.text = surveyData![carousel.currentItemIndex]["title"] as? String
            
            // set description
            descLabel.text = surveyData![carousel.currentItemIndex]["description"] as? String
            
        }
    }
    
}



// MARK: - Survey Delegate
// ********************************** SURVEY DELEGATE **************************************** //
extension MainViewController: SurveyDelegate {
    
    func SurveyGetAllData(data: AnyObject?) {
        
        if data != nil {
            
            surveyData = Survey.sharedInstance.survayData
            
            pageControl.numberOfPages = surveyData!.count
            
        }
    }
}
