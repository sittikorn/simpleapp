//
//  SurveyViewController.swift
//  SimpleApp
//
//  Created by Sittikorn on 1/4/2559 BE.
//  Copyright © 2559 Sittikorn. All rights reserved.
//

import UIKit

class SurveyViewController: UIViewController {
    

    override func viewDidLoad() {
        super.viewDidLoad()

        createTitleView()
        createCloseButton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // ************************* CREATE TITLE VIEW ***************************** //
    
    func createTitleView() {
        
        let titleLabel = UILabel(frame: CGRectZero)
        titleLabel.text = "SURVEYS"
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.font = UIFont.boldSystemFontOfSize(22);
        titleLabel.sizeToFit()
        
        navigationItem.titleView = titleLabel
        
    }

    
    // ********************************** CUSTOM CLOSE BUTTOM **************************************** //
    func createCloseButton() {
        
        let closeButton = UIButton(frame: CGRectMake(0,0,30,30))
        closeButton.setImage(UIImage(named: "Closebutton"), forState: UIControlState.Normal)
        closeButton.tintColor = UIColor.whiteColor()
        closeButton.addTarget(self, action: Selector("backAction:"), forControlEvents: UIControlEvents.TouchUpInside)
        
        let closeBar = UIBarButtonItem(customView: closeButton)
        
        navigationItem.leftBarButtonItem = closeBar
    }
    
    func backAction(sender:UIBarButtonItem) {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
