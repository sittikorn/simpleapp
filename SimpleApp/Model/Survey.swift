//
//  Survay.swift
//  SimpleApp
//
//  Created by Sittikorn on 1/4/2559 BE.
//  Copyright © 2559 Sittikorn. All rights reserved.
//

import Foundation
import Alamofire

@objc protocol SurveyDelegate {
    
    optional func SurveyGetAllData(data:AnyObject?)
}

class Survey {
    
    var survayData:AnyObject?
    
    class var sharedInstance: Survey {
        struct Singleton {
            static let instance = Survey()
        }
        return Singleton.instance
    }
    
    /*
    *  name :   getAllSurvey
    *  des :    get all survey data
    *  param :  delegate = SettingDelegate
    */
    func getAllSurvay(delegate:SurveyDelegate) {
        
        Alamofire.request(.GET, "https://www-staging.usay.co/app/surveys.json", parameters: ["access_token": "6eebeac3dd1dc9c97a06985b6480471211a777b39aa4d0e03747ce6acc4a3369"])
            .responseJSON { response in
                
                if let json = response.result.value {
                    
                    self.survayData = json
                    
                    delegate.SurveyGetAllData!(self.survayData)
                }
        }
    }
}